const int FULL_POWER = 255;

struct TravelState {
  bool IsTraveling;
  bool DirectionForward;
  unsigned int DesiredSteps;
  bool IsDesiredReached;
};

class Motor {
    int RPWMOutput;
    int LPWMOutput;
    byte FeedbackPin;
    unsigned int CurrentSteps;
    float StepDistance;
    TravelState State;
    char Name;
    unsigned long _interruptionInterval;
    unsigned long _interruptionIntervalAim;
    unsigned long _lastInterruption;
    int _power;
    int _newPower;
  public:
    Motor() {}
    Motor(char N, int RPin, int LPin, byte FPin, float stepDistance, unsigned int currentSteps) {
      Name = N;
      RPWMOutput = RPin;
      LPWMOutput = LPin;
      FeedbackPin = FPin;
      CurrentSteps = currentSteps;
      StepDistance = stepDistance;

      _lastInterruption = 0;
      _power = FULL_POWER;
      _interruptionIntervalAim = 0;

      pinMode(RPWMOutput, OUTPUT);
      pinMode(LPWMOutput, OUTPUT);
      pinMode(FeedbackPin, INPUT_PULLUP);
    }

    void SetInterrupt(void (*interrupt)()) {
      attachInterrupt(digitalPinToInterrupt(FeedbackPin), interrupt, FALLING);
    }

    void Interrupt() {

      unsigned long interruptTime = millis();
      
//      Serial.print(Name);
//      Serial.println(" interrupted");
//
//      Serial.print("CurrentSteps");
//      Serial.println(CurrentSteps);
//      Serial.print("DesiredSteps");
//      Serial.println(State.DesiredSteps);

      if (_lastInterruption > 0) {
        _interruptionInterval = interruptTime - _lastInterruption;
      }

      _lastInterruption = interruptTime;
      
      if (State.IsTraveling) {
        if (State.DirectionForward) {
          CurrentSteps += 1;

          if (CurrentSteps >= State.DesiredSteps) {
            State.IsDesiredReached = true;
            Stop();
          }
        } else {
          CurrentSteps -= 1;
          if (CurrentSteps <= State.DesiredSteps) {
            State.IsDesiredReached = true;
            Stop();
          }
        }
      }

      if (_interruptionIntervalAim > 0) {
        if (_interruptionInterval > 0) {
          float percent = _interruptionIntervalAim * 100 / _interruptionInterval;
          int newpower = (_power * percent) / 100;
          if (newpower > (FULL_POWER - 50)) {
            if (newpower > FULL_POWER) {
              _power = FULL_POWER;
            } else {
              _power = newpower;
            }
          }
        }
      }
      
    }

    void SetDesiredPosition(unsigned int p) {
      int wantedSteps =  p/StepDistance;
      SetDesiredSteps(wantedSteps);
    }

    void SetDesiredSteps(unsigned int steps) {
      if (State.DesiredSteps != steps) {
        State.IsDesiredReached = false;
        State.DesiredSteps = steps;
      }
    }

    void Sync() {
      if (State.IsTraveling) {
//        Serial.print("Motor ");
//        Serial.print(Name);
//        Serial.println(" is traveling already. Skipping.");
        return;
      }

      if (State.IsDesiredReached) {
//        Serial.print("Motor ");
//        Serial.print(Name);
//        Serial.println(" is on desired step already. Skipping.");
        return;
      }

      int posDiff = State.DesiredSteps - CurrentSteps;

      if (posDiff > 0) {
        Forward();
      }

      if (posDiff < 0) {
        Reverse();
      }
    }

    unsigned int StepsToPosition() {
      return CurrentSteps * StepDistance;
    }
    
    void Forward() {
      Serial.print("Forwarding motor ");
      Serial.println(Name);
      Serial.print("Power: ");
      Serial.println(_power);
      analogWrite(RPWMOutput, 0);
      analogWrite(LPWMOutput, _power);
      State.IsTraveling = true;
      State.DirectionForward = true;
    }

    void Reverse() {
      Serial.print("Reversing motor ");
      Serial.println(Name);
      Serial.print("Power: ");
      Serial.println(_power);
      analogWrite(RPWMOutput, _power);
      analogWrite(LPWMOutput, 0);
      State.IsTraveling = true;
      State.DirectionForward = false;
    }

    void Stop() {
      Serial.print("Stopping motor ");
      Serial.println(Name);
      analogWrite(RPWMOutput, 0);
      analogWrite(LPWMOutput, 0);
      State.IsTraveling = false;
    }

    unsigned int GetCurrentSteps() {
      return CurrentSteps;
    }
    unsigned int GetDesiredSteps() {
      return State.DesiredSteps;
    }
    char GetName() {
      return Name;  
    }
    unsigned int GetCurrentInterruptionInterval() {
      return _interruptionInterval;
    }
    void SetInterruptionIntervalAim(unsigned int aim) {
      _interruptionIntervalAim = aim;
    }
    void Print() {
      Serial.print("Motor ");
      Serial.print(GetName());
      Serial.print(" - ");
    }
    void PrintSpeed() {
      Print();
      Serial.print("CurrentInterruptionInterval: ");
      Serial.println(GetCurrentInterruptionInterval());
      Serial.print("GetCurrentSteps: ");
      Serial.println(GetCurrentSteps());
    }
};
