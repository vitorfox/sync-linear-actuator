int buttons_Input = A0;
int led_1_Input = 8;
int led_2_Input = 9;

int buttons_value = 0;
int pressed_button = 0;

int UP = 1;
int DOWN = 2;
int LEFT = 3;
int RIGHT = 4;
int SELECT = 5; 

void setup_control() {
  pinMode(led_1_Input, OUTPUT);
  pinMode(led_2_Input, OUTPUT);

  digitalWrite(led_1_Input, HIGH);  
}

void load_pressed_button() {
  buttons_value = analogRead(buttons_Input);
//  Serial.print("buttons_value:");
//  Serial.println(buttons_value);

  if (buttons_value < 30) {
    pressed_button = 0;
    digitalWrite(led_2_Input, LOW);
    return;
  }

  if (buttons_value < 120) {
    digitalWrite(led_2_Input, HIGH);
    pressed_button = DOWN;
    return;
  }

  if (buttons_value < 200) {
    digitalWrite(led_2_Input, HIGH);
    pressed_button = LEFT;
    return;
  }  

  if (buttons_value < 300) {
    digitalWrite(led_2_Input, HIGH);
    pressed_button = SELECT ;
    return;
  }

  if (buttons_value < 500) {
    digitalWrite(led_2_Input, HIGH);
    pressed_button = RIGHT;
    return;
  }
  
  if (buttons_value > 1000) {
    digitalWrite(led_2_Input, HIGH);
    pressed_button = UP;
    return;
  }

  pressed_button = 0;
  digitalWrite(led_2_Input, LOW);

}

void check_control() {
  load_pressed_button();
//  Serial.println(pressed_button);
}
