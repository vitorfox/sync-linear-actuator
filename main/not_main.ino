int motorAFeedback_Input = 2;
int motorARPWM_Output = 3;
int motorALPWM_Output = 4;

int motorBFeedback_Input = 1;
int motorBRPWM_Output = 6;
int motorBLPWM_Output = 7;


unsigned int _startingStepsA = 0;
unsigned int _startingStepsB = 0;
unsigned int _desiredSteps = 0;

float stepDistance = 10;

//Motor motorX('X', motorXRPWM_Output, motorXLPWM_Output, motorXFeedback_Input, stepDistance, _startingStepsX);

Syncer syncer;

Motor motorA;
Motor motorB;

void setup()
{
  setup_control();
  motorA = Motor('A', motorARPWM_Output, motorALPWM_Output, motorAFeedback_Input, stepDistance, _startingStepsA);
  motorB = Motor('B', motorBRPWM_Output, motorBLPWM_Output, motorBFeedback_Input, stepDistance, _startingStepsB);

  motorA.SetInterrupt(interruptA);
  motorB.SetInterrupt(interruptB);

  syncer.AddMotor(&motorA);
  syncer.AddMotor(&motorB);
  syncer.SetSlowerMotor(0);
  
  Serial.println("Done setup");
}

void interruptA() {
  Serial.println("interruptA");
  motorA.Interrupt();
}

void interruptB() {
  Serial.println("interruptB");
  motorB.Interrupt();
}

void loop()
{
//  syncer.SetDesiredSteps(_desiredSteps);

  check_control();
//  syncer.Sync();
//  motorA.Sync();
//  motorB.Sync();
  
  delay(50);
}
