struct SyncerState {
  unsigned int DesiredSteps;
  bool DirectionForward;
  bool IsDesiredReached;
};

class Syncer {
  unsigned int minStep;
  SyncerState state;
  unsigned int motorCount;
  unsigned int slower;

  int _diff, _i;
  unsigned int _maxStepDistance, _absD;
  
  Motor *motors[];
  public:
    Syncer() {
      motorCount = 0;
    }
    void AddMotor(Motor *motor) {
      motors[motorCount] = motor;
      motorCount++;
    }
    void SetSlowerMotor(unsigned int s) {
      slower = s;
    }
    void Sync() {

      _diff = state.DesiredSteps - motors[0]->GetCurrentSteps();
      _maxStepDistance = realAbs(_diff);
  
      for (_i = 1; _i < motorCount; _i++) {
        _diff = state.DesiredSteps - motors[_i]->GetCurrentSteps();
        _absD = realAbs(_diff);
        if (_absD > _maxStepDistance) {
          _maxStepDistance = _absD;
        }
      }
      
      for (_i = 0; _i < motorCount; _i++) {
        _diff = state.DesiredSteps - motors[_i]->GetCurrentSteps();
        _absD = realAbs(_diff);

        motors[_i]->PrintSpeed();
//          Serial.print("Motor ");
//          Serial.print(motors[_i]->GetCurrentSteps())
//          Serial.println(motors[_i]->GetCurrentSteps())

        if (_i != slower) {
          if (motors[_i]->GetCurrentInterruptionInterval() > motors[slower]->GetCurrentInterruptionInterval()) {
            motors[_i]->SetInterruptionIntervalAim(motors[slower]->GetCurrentInterruptionInterval());
          }
        }
        
        if (_absD < _maxStepDistance) {
//          Serial.print("maxStepDistance:");
//          Serial.println(_maxStepDistance);
//          Serial.print("absD:");
//          Serial.println(_absD);
//          Serial.print("state.DesiredSteps:");
//          Serial.println(state.DesiredSteps);
          motors[_i]->Stop();
        } else {
//          Serial.print("maxStepDistance:");
//          Serial.println(_maxStepDistance);
//          Serial.print("absD:");
//          Serial.println(_absD);
//          Serial.print("state.DesiredSteps:");
//          Serial.println(state.DesiredSteps);
//          Serial.print("CurrentSteps:");
//          Serial.println(motors[_i]->GetCurrentSteps());
//          Serial.println("Sync");
          motors[_i]->Sync();
        }
      }
    }

    void SetDesiredSteps(unsigned int steps) {
      if (steps != state.DesiredSteps) {
        state.DesiredSteps = steps;
        state.IsDesiredReached = false;
      }

      for (int i = 0; i < motorCount; i++) {
        Serial.print("Setting desired steps for motor ");
        Serial.println(motors[i]->GetName());
        motors[i]->SetDesiredSteps(steps);
        Serial.println(motors[i]->GetDesiredSteps());
      }      
    } 
};
